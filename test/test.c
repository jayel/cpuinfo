#include "cpuinfo.h"
#include "cpuinfo_types.h"
#include <stdio.h>
#include <string.h>

int main()
{
    union intel_cpu_info info = { 0 };
    union intel_misc_info misc = { 0 };
    union intel_ecx_features ecx = { 0 };
    union intel_edx_features edx = { 0 };
    struct regs registers;
    int32_t ecx_index = 0;
    char cpu_name[64];

    memset(cpu_name, 0, 64);
    get_cpu_name(cpu_name);
    printf("CPUID available: %d\n", cpuid_available());
    printf("GenuineIntel: %d\n", is_genuine_intel());
    printf("CPU Name: %s\n", cpu_name);
    if (!cpuid_available() || !is_genuine_intel()) {
        printf("error\n");
        return 1;
    }
    e_cpuid(1, ecx_index, &registers);
    info.value = registers.eax;
    misc.value = registers.ebx;
    ecx.value = registers.ecx;
    edx.value = registers.edx;
    printf("max basic eax: %X, max extended eax: %X\n", highest_eax_basic_info(), highest_eax_extended_info());
    printf("family: %d, model: %d, stepping: %d\n", info.flags.family_code, info.flags.model_number, info.flags.stepping);
    printf("brand id: %X, apic id: %X, logical processor count: %d\n", misc.flags.brand_id, misc.flags.apic_id, misc.flags.count);
    printf("sse3: %d\nsse4.1: %d\nsse4.2: %d\nvmx: %d\npopcnt: %d\naes: %d\n", ecx.flags.sse3, ecx.flags.sse41, ecx.flags.sse42, ecx.flags.vmx, ecx.flags.popcnt, ecx.flags.aes);
    printf("htt: %d\n", edx.flags.htt);

    registers.eax = 0;
    registers.ebx = 0;
    registers.ecx = 0;
    registers.edx = 0;
    ecx_index = 0;
    while (1) {
        e_cpuid(4, ecx_index, &registers);
        if (!(registers.eax & 0x1F)) {
            printf("Done (EAX = %X)\n", registers.eax);
            break;
        }
        printf("EAX=04h, Leaf n=%d, level=%d\n", ecx_index, (registers.eax >> 5) & 0x7);
        printf("Max # of addressable IDs for logical processors sharing this cache: %d\n", 1 + ((registers.eax >> 14) & 0xFFF));
        printf("Max # of addressable IDs for processors cores in physical package: %d\n", 1 + ((registers.eax >> 26) & 0x3F));
        ++ecx_index;
    }

    if (highest_eax_basic_info() < 0xb)
        return 0;

    ecx_index = 0;
    while (1) {
        e_cpuid(0xb, ecx_index, &registers);
        if (registers.eax == 0 && registers.ebx == 0)
            break;
        if (ecx_index > 8)
            break;
        printf("EAX=0Bh, Extended topology leaf n=%d\n", ecx_index);
        printf("x2APIC ID bits to shift right to get unique topology ID: %d\n", registers.eax & 0xFFFF);
        printf("Logical processors at this level type: %d\n", registers.ebx & 0xFFFF);
        printf("Level number: %d ", registers.ecx & 0xFF);
        switch (registers.ecx & 0xFF) {
            case 1: printf("(Thread)\n"); break;
            case 2: printf("(Core)\n"); break;
            case 3: printf("(Package)\n"); break;
            default: printf("(NULL)\n"); break;
        }
        ++ecx_index;
    }
    return 0;
}
