# cpuinfo

*cpuinfo* is a library to easily retrieve CPU information such as features, model, etc. (as found in the various `CPUID` modes), in C and C++ code.
The library supports GNU/Linux and Windows in 32-bit or 64-bit. Other operating systems haven't been tested yet.

## Compiling

### Prerequisites for Linux:

- cmake >= 2.9
- Portable GNU assembler (as)
- gcc

### Prerequisites for Windows:

- cmake >= 2.9
- Visual Studio 2008 (?) or newer

Note on CMake & Windows

    CMake seems to be bugged at this moment (2.8.9). When using NMake Makefiles as the 
    generator, when it generates the commands to build a static library from assembly 
    files in build.make it creates a line with stuff like this:

        "" cr cpuinfo.lib  $(cpuinfo_OBJECTS) $(cpuinfo_EXTERNAL_OBJECTS) 

    Meanwhile the file build.make in cpuinfo.dir/ has to be modified manually with the following:

        lib  $(cpuinfo_OBJECTS) $(cpuinfo_EXTERNAL_OBJECTS) /OUT:cpuinfo.lib
