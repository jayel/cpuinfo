#ifndef CPUINFO_PLATFORM_H
#define CPUINFO_PLATFORM_H

// _WIN32 is defined in both 64 and 32-bit Windows versions, so first check
// for 64 and if it's not defined check for 32-bit.
#ifdef _WIN64
#   define EXPORT
#elif defined _WIN32
#   define EXPORT __stdcall
#else
#   define EXPORT
#endif

#endif
