#ifndef CPUINFO_CPUID_H
#define CPUINFO_CPUID_H

#include "platform.h"
#include <stdint.h>

// Returns a positive integer if CPUID is available, 0 otherwise.
int32_t EXPORT cpuid_available();

// Returns a positive integer if this is an Intel CPU.
// Otherwise returns 0.
int32_t EXPORT is_genuine_intel();

// Returns a positive integer if this is an AMD CPU.
// Otherwise returns 0.
int32_t EXPORT is_authentic_amd();

// Returns a positive integer with the highest value EAX can have when calling CPUID for basic
// information (EAX = 0000 000X).
// Calling CPUID with a value in EAX higher than this is undefined.
int32_t EXPORT highest_eax_basic_info();

// Returns a positive integer with the highest value EAX can have when calling CPUID for extended
// information (EAX = 8000 000X).
// Calling CPUID with a value in EAX higher than this is undefined.
int32_t EXPORT highest_eax_extended_info();

// Calls CPUID with the registers eax and ecx set to 'eax' and 'ecx', and returns the value 
// of E[A..D]X into registers.
// 'registers' has to be at least 16 bytes wide.
void EXPORT e_cpuid(int32_t eax, int32_t ecx, void* registers);

// Copies the processor brand string for x86 CPUS into 'dest'.
// The string is null terminated and has no leading spaces.
void EXPORT get_cpu_name(char* dest);


#endif
