#ifndef CPUINFO_TYPES_H
#define CPUINFO_TYPES_H

#include <stdint.h>

struct intel_cpu_info_flags
{
    uint32_t stepping:4;
    uint32_t model_number:4;
    uint32_t family_code:4;
    uint32_t type:2;
    uint32_t reserved2:2;
    uint32_t extended_model:4;
    uint32_t extended_family:8;
    uint32_t reserved1:4;
};

struct intel_misc_info_flags
{
    uint32_t brand_id:8;
    uint32_t chunks:8;
    uint32_t count:8;
    uint32_t apic_id:8;
};

struct intel_ecx_feature_flags
{
    uint32_t sse3:1;                // SSE3 instructions
    uint32_t pclmuldq:1;            // PCLMULDQ instruction
    uint32_t dtes64:1;              // 64-bit debug store
    uint32_t monitor:1;             // MONITOR/MWAIT
    uint32_t ds_cpl:1;              // CPL qualified debug store
    uint32_t vmx:1;                 // Virtual machine extensions (Intel Virtualization)
    uint32_t smx:1;                 // Safer mode extensions (Intel Trusted Execution)
    uint32_t eist:1;                // Enhanced Intel SpeedStep
    uint32_t tm2:1;                 // Thermal monitor 2
    uint32_t ssse3:1;               // Supplemental SSE3 instructions
    uint32_t cnxt_id:1;             // L1 context id
    uint32_t reserved0:1;
    uint32_t fma:1;                 // Fused multiply add
    uint32_t cx16:1;                // CMPXCHG16B
    uint32_t xtpr:1;                // xTPR Update control
    uint32_t pdcm:1;                // Perfmon debug capability
    uint32_t reserved1:1;
    uint32_t pcid:1;                // Process context identifiers
    uint32_t dca:1;                 // Direct cache access
    uint32_t sse41:1;               // SSE4.1 instructions
    uint32_t sse42:1;               // SSE4.2 instructions
    uint32_t x2apic:1;              // Extended xAPIC support
    uint32_t movbe:1;               // MOVBE instruction
    uint32_t popcnt:1;              // POPCNT instruction
    uint32_t tsc_deadline:1;        // Time stamp counter deadline
    uint32_t aes:1;                 // AES instructions
    uint32_t xsave:1;               // XSAVE/XSTOR states
    uint32_t osxsave:1;             // Os-enabled extended state management
    uint32_t avx:1;                 // Advanced vector extensions
    uint32_t f16c:1;                // 16-bit floating point conversion instructions
    uint32_t rdrand:1;              // RDRAND instruction
    uint32_t reserved2:1;
};

struct intel_edx_feature_flags
{
    uint32_t fpu:1;                 // Floating point unit on chip
    uint32_t vme:1;                 // Virtual mode extension
    uint32_t de:1;                  // Debugging extension
    uint32_t pse:1;                 // Page size extension
    uint32_t tsc:1;                 // Time stamp counter
    uint32_t msr:1;                 // Model specific registers
    uint32_t pae:1;                 // Physical address extension
    uint32_t mce:1;                 // Machine-check exception
    uint32_t cx8:1;                 // CMPXCHG8 instruction
    uint32_t apic:1;                // On-chip APIC hardware
    uint32_t reserved0:1;
    uint32_t sep:1;                 // Fast system call
    uint32_t mtrr:1;                // Memory type range registers
    uint32_t pge:1;                 // Page global enable
    uint32_t mca:1;                 // Machine-check architecture
    uint32_t cmov:1;                // Conditional move instructions
    uint32_t pat:1;                 // Page attribute table
    uint32_t pse_36:1;              // 36-bit page size extension
    uint32_t psn:1;                 // Processor serial number present and enabled
    uint32_t clfsh:1;               // CLFLUSH instruction
    uint32_t reserved1:1;
    uint32_t ds:1;                  // Debug store
    uint32_t acpi:1;                // Thermal monitor and software controlled clock facilities
    uint32_t mmx:1;                 // MMX extensions
    uint32_t fxsr:1;                // FXSAVE and FXSTOR instructions
    uint32_t sse:1;                 // SSE extensions
    uint32_t sse2:1;                // SSE2 extensions
    uint32_t ss:1;                  // Self snoop
    uint32_t htt:1;                 // Multi-threading
    uint32_t tm:1;                  // Thermal monitor
    uint32_t reserved2:1;
    uint32_t pbe:1;                 // Pending break enable
};

struct intel_extended_ecx_feature_flags
{
    uint32_t lahf:1;                // LAFH/SAHF instructions
    uint32_t reserved:31;
};

struct intel_extended_edx_feature_flags
{
    uint32_t reserved0:11;
    uint32_t syscall:1;             // SYSCALL/SYSRET instructions
    uint32_t reserved1:8;
    uint32_t xd:1;                  // Execution disable bit
    uint32_t reserved2:5;
    uint32_t one_gb_pages:1;        // 1GB pages
    uint32_t rdtscp:1;              // RDTSCP and I32_TSC_AUX
    uint32_t reserved3:1;
    uint32_t intel64:1;             // Intel 64 instruction set
    uint32_t reserved4:2;
};

struct intel_deterministic_cache_parameter_flags
{
    uint32_t reserved0:14;
    uint32_t threads:12;
    uint32_t cores:6;
};

// EAX = 0x1
// EAX = 0x8000 0001
struct amd_cpu_info_flags
{
    uint32_t settping:4;
    uint32_t base_model:4;
    uint32_t base_family:4;
    uint32_t reserved0:4;
    uint32_t extended_model:4;
    uint32_t extended_family:8;
    uint32_t reserver1:4;
};

// EAX = 0x1
struct amd_misc_info_flags
{
    uint32_t brand_id:8;
    uint32_t cflush_size:8;
    uint32_t logical_processor_count:8;
    uint32_t apic_id:8;
};

// EAX = 0x1
struct amd_ecx_feature_flags
{
    uint32_t sse3:1;            // SSE3 instructions
    uint32_t pclmulqdq:1;       // PCLMULQDQ instruction
    uint32_t reserved0:1;
    uint32_t monitor:1;         // MONITOR/MWAIT instructions
    uint32_t reserved1:5;
    uint32_t ssse3:1;           // Supplemental SSE3 instructions
    uint32_t reserved2:2;
    uint32_t fma:1;             // FMA instruction
    uint32_t cmpxchg16b:1;      // CMPXCHG16B instruction
    uint32_t reserved3:5;
    uint32_t sse41:1;           // SSE4.1 instructions
    uint32_t sse42:1;           // SSE4.2 instructions
    uint32_t reserved4:2;
    uint32_t popcnt:1;          // POPCNT instruction
    uint32_t reserved5:1;
    uint32_t aes:1;             // AES instructions
    uint32_t xsave:1;           // XSAVE (and related) instructions
    uint32_t osxsave:1;         // OSXSAVE (and related) instructions
    uint32_t avx:1;             // AVX instructions
    uint32_t f16c:1;            // half-precision convert instruction support
    uint32_t reserved6:1;
    uint32_t guest_status:1;    // Reserved for use by hypervisor to indicate guest status
};

// EAX = 0x1
struct amd_edx_feature_flags
{
    uint32_t fpu:1;             // x87 floating point unit on-chip
    uint32_t vme:1;             // Virtual-mode enhancements
    uint32_t de:1;              // Debugging extensions
    uint32_t pse:1;             // Page size extensions
    uint32_t tsc:1;             // Time stamp counter
    uint32_t msr:1;             // AMD model-specific registers
    uint32_t pae:1;             // Physical-address extensions
    uint32_t mce:1;             // Machine check extension
    uint32_t cmpxchg8b:1;       // CMPXCHG8B instruction
    uint32_t apic:1;            // Advanced programmable interrupt controller
    uint32_t reserved0:1;
    uint32_t fast_syscall:1;    // SYSENTER and SYSEXIT
    uint32_t mtrr:1;            // Memory-type range registers
    uint32_t pge:1;             // Page global extension
    uint32_t mca:1;             // Machine check architecture
    uint32_t cmov:1;            // Conditional move instructions
    uint32_t pat:1;             // Page attribute table
    uint32_t pse36:1;           // Page size extensions
    uint32_t reserved1:1;
    uint32_t clfsh:1;           // CLFLUSH instruction
    uint32_t reserved2:3;
    uint32_t mmx:1;             // MMX instructions
    uint32_t fxsr:1;            // FXSAVE and FXRSTOR instructions
    uint32_t sse:1;             // SSE instructions
    uint32_t sse2:1;            // SSE2 instructions
    uint32_t reserved3:1;
    uint32_t htt:1;             // Hyper-threading technology
    uint32_t reserved4:3;
};

// EAX = 0x8000 0001
// Value is in EBX
struct amd_extended_brand_id_flags
{
    uint32_t brand_id:16;
    uint32_t reserved0:12;
    uint32_t package_type:4;
};

// EAX = 0x8000 0001
struct amd_extended_ecx_feature_flags
{
    uint32_t lahfsahf:1;                // LAHF and SAHF instruction support in 64-bit mode
    uint32_t cmp_legacy:1;              // Core multi-processing legacy mode
    uint32_t svm:1;                     // Secure virtual machine
    uint32_t extended_apic:1;           // Extended APIC space
    uint32_t alt_mov:1;                 // LOCK MOV CR0 means MOV CR8
    uint32_t abm:1;                     // Advanced bit manipulation
    uint32_t sse4a:1;                   // EXTRQ, INSERTQ, MOVNTSS, and MOVNTSD instructions
    uint32_t misaligned_sse:1;          // Misaligned SSE mode
    uint32_t _3dnow_prefetch:1;         // PREFETCH and PREFETCHW instructions
    uint32_t osvw:1;                    // OS visible workaround
    uint32_t ibs:1;                     // Instruction based sampling
    uint32_t xop:1;                     // Extended operation support
    uint32_t skinit:1;                  // SKINIT and STGI supported
    uint32_t wdt:1;                     // Watchdog timer support
    uint32_t reserved0:1;
    uint32_t lwp:1;                     // Lighweight profiling support
    uint32_t fma4:1;                    // 4-operand FMA instructions
    uint32_t reserved1:2;
    uint32_t node_id:1;                 // Support for MSRC001_100C
    uint32_t reserved2:1;
    uint32_t tbm:1;                     // Trailing bit manipulation instructions
    uint32_t topology_extensions:1;     // Support for CPUID EAX = 0x8000 001D
    uint32_t reserved3:9;
};

struct amd_extended_edx_feature_flags
{
    uint32_t fpu:1;             // x87 floating point unit on-chip
    uint32_t vme:1;             // Virtual-mode enhancements
    uint32_t de:1;              // Debugging extensions
    uint32_t pse:1;             // Page size extensions
    uint32_t tsc:1;             // Time stamp counter
    uint32_t msr:1;             // AMD model-specific registers
    uint32_t pae:1;             // Physical-address extensions
    uint32_t mce:1;             // Machine check extension
    uint32_t cmpxchg8b:1;       // CMPXCHG8B instruction
    uint32_t apic:1;            // Advanced programmable interrupt controller
    uint32_t reserved0:1;
    uint32_t fast_syscall:1;    // SYSENTER and SYSEXIT
    uint32_t mtrr:1;            // Memory-type range registers
    uint32_t pge:1;             // Page global extension
    uint32_t mca:1;             // Machine check architecture
    uint32_t cmov:1;            // Conditional move instructions
    uint32_t pat:1;             // Page attribute table
    uint32_t pse36:1;           // Page size extensions
    uint32_t reserved1:2;
    uint32_t nx:1;              // No-execute page protection
    uint32_t reserved2:1;
    uint32_t mmx_extensions:1;  // AMD extensions to MMX
    uint32_t mmx:1;             // MMX instructions
    uint32_t fxsr:1;            // FXSAVE and FXRSTOR instructions
    uint32_t ffxsr:1;           // FXSAVE and FXRSTOR optimizations
    uint32_t page_1gb:1;        // 1GB page support
    uint32_t rdtscp:1;          // RDTSCP instruction
    uint32_t reserved:1; 
    uint32_t lm:1;              // Long mode
    uint32_t _3dnow_ext:1;      // AMD extensions to 3DNow
    uint32_t _3dnow:1;          // 3DNow instructions
};

#define UINT32_UNION_TEMPLATE(name, type) union name {\
    uint32_t value;\
    type flags; }

UINT32_UNION_TEMPLATE(intel_cpu_info, struct intel_cpu_info_flags);
UINT32_UNION_TEMPLATE(intel_misc_info, struct intel_misc_info_flags);
UINT32_UNION_TEMPLATE(intel_ecx_features, struct intel_ecx_feature_flags);
UINT32_UNION_TEMPLATE(intel_edx_features, struct intel_edx_feature_flags);
UINT32_UNION_TEMPLATE(intel_extended_ecx_features, struct intel_extended_ecx_feature_flags);
UINT32_UNION_TEMPLATE(intel_extended_edx_features, struct intel_extended_edx_feature_flags);
UINT32_UNION_TEMPLATE(intel_dca_parameters, struct intel_deterministic_cache_parameter_flags);

UINT32_UNION_TEMPLATE(amd_cpu_info, struct amd_cpu_info_flags);
UINT32_UNION_TEMPLATE(amd_misc_info, struct amd_misc_info_flags);
UINT32_UNION_TEMPLATE(amd_ecx_features, struct amd_ecx_feature_flags);
UINT32_UNION_TEMPLATE(amd_edx_features, struct amd_edx_feature_flags);
UINT32_UNION_TEMPLATE(amd_extended_brand_id, struct amd_extended_brand_id_flags);
UINT32_UNION_TEMPLATE(amd_extended_ecx_features, struct amd_extended_ecx_feature_flags);
UINT32_UNION_TEMPLATE(amd_extended_edx_features, struct amd_extended_edx_feature_flags);

struct regs
{
    int32_t eax;
    int32_t ebx;
    int32_t ecx;
    int32_t edx;
};

#endif
