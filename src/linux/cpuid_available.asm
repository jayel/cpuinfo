#include "cpuid_available.h"


.section .text
.globl cpuid_available, highest_eax_basic_info, highest_eax_extended_info, is_genuine_intel, is_authentic_amd
.type cpuid_available, @function
.type highest_eax_basic_info, @function
.type highest_eax_extended_info, @function
.type is_genuine_intel, @function
.type is_authentic_amd, @function
cpuid_available:
    pushq %rbx
    movl $0x200000, %ebx
    pushfq
    pop %rax
    push %rax
    xor %ebx, %eax
    push %rax
    popfq
    pushfq
    pop %rcx
    xor %ecx, %eax
    xor %ebx, %eax
    popfq
    popq %rbx
    ret

highest_eax_basic_info:
    pushq %rbx
    movl $0x0, %eax
    cpuid
    popq %rbx
    ret

highest_eax_extended_info:
    pushq %rbx
    movl $0x80000000, %eax
    cpuid
    popq %rbx
    ret

is_genuine_intel:
    pushq %rbx
    movl $0x0, %eax
    cpuid
    cmp $0x756e6547, %ebx
    jne vendor_different
    cmp $0x49656e69, %edx
    jne vendor_different
    cmp $0x6c65746e, %ecx
    jne vendor_different
    mov $0x1, %eax
    popq %rbx
    ret

is_authentic_amd:
    pushq %rbx
    movl $0x0, %eax
    cpuid
    cmp $0x68747541, %ebx
    jne vendor_different
    cmp $0x69746e65, %edx
    jne vendor_different
    cmp $0x444d4163, %ecx
    jne vendor_different
    popq %rbx
    ret

vendor_different:
    mov $0x0, %eax
    popq %rbx
    ret

.data
