#include "cpuinfo.h"

    .section .text
    .globl e_cpuid
    .type e_cpuid, @function
e_cpuid:
.LFB0:
    pushq %rbp
    movq %rsp, %rbp
    /* Save rbx and the destination pointer since it will changed by cpuid */
    pushq %rbx
    pushq %rdx
    /* Copy input arguments for cpuid */
    movl %edi, %eax
    movl %esi, %ecx
    cpuid
    /* Pop destination pointer to r8 and copy the value of the registers
       into it. */
    popq %r8
    movl %eax, 0(%r8)
    movl %ebx, 4(%r8)
    movl %ecx, 8(%r8)
    movl %edx, 12(%r8)
    /* Get original values for rbx and rbp */
    popq %rbx
    popq %rbp
    ret
.LFE0:
    .size e_cpuid, .-e_cpuid

    .globl get_cpu_name
    .type get_cpu_name, @function
get_cpu_name:
.LFB1:
    pushq %rbp
    movq %rsp, %rbp
    /* Preserve original values of rbx, rdi */
    pushq %rbx
    pushq %rdi
    /* Claim the red zone to store a local char array */
    subq $0x80, %rsp
    movq %rsp, %r8
    movq $0, %r9
    /* Call CPUID with EAX=2,3,4 and save the characters into the local array */
    movl $0x80000002, %eax
    cpuid
    call copy_brand_string
    addq $0x10, %r9
    movl $0x80000003, %eax
    cpuid
    call copy_brand_string
    addq $0x10, %r9
    movl $0x80000004, %eax
    cpuid
    call copy_brand_string
    /* Initialise offsets for rdi (rcx) and r8 (r9) */
    movq $0x0, %rcx
    movq $0x0, %r9
start_skip_leading_spaces:
    /* Increase offset until we find a character that isn't a space or we reach
       the end of the array. The string returned by CPUID is always 48 characters
       long, with leading spaces.  */
    movb (%r8, %r9), %al
    cmp $0x20, %al
    jne copy_cpu_name_to_dest
    inc %r9
    cmpq $0x30, %r9
    jl start_skip_leading_spaces
    /* Copy the rest of the string one byte at a time. Can be improved in a few
       different ways (use movq, or the string operations), but this works for
       now.  */
start_copy_cpu_name_to_dest:
    movb (%r8, %r9), %al
copy_cpu_name_to_dest:
    movb %al, (%rdi, %rcx)
    inc %r9
    inc %rcx
    cmpq $0x30, %r9
    jl start_copy_cpu_name_to_dest
    /* Release local memory and pop original values for used registers. */
done:
    addq $0x80, %rsp
    popq %rdi
    popq %rbx
    popq %rbp
    ret
.LFE1:
    .size get_cpu_name, .-get_cpu_name

    .type copy_brand_string, @function
copy_brand_string:
.LFB2:
    movl %eax, (%r8, %r9)
    movl %ebx, 4(%r8, %r9)
    movl %ecx, 8(%r8, %r9)
    movl %edx, 12(%r8, %r9)
    ret
.LFE2:
    .size copy_brand_string, .-copy_brand_string
