.586
.model flat, stdcall
option casemap :none
.code

e_cpuid proc export arg1:DWORD, arg2:DWORD, arg3:PTR
    push ebp
    mov ebp, esp
    push ebx
    push edi
    mov eax, dword ptr [ebp+12]
    mov ecx, dword ptr [ebp+16]
    cpuid
    mov edi, dword ptr [ebp+20]
    mov dword ptr [edi], eax
    mov dword ptr [edi+4], ebx
    mov dword ptr [edi+8], ecx
    mov dword ptr [edi+12], edx
    pop edi
    pop ebx
    pop ebp
    ret
e_cpuid endp

end
