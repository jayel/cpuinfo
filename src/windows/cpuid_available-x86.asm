.586
.model flat, stdcall
option casemap :none
.code

cpuid_available proc public
    push ebp
    mov ebp, esp
    push ebx
    mov ebx, 200000h
    pushf
    pop eax
    push eax
    xor eax, ebx
    push eax
    popf
    pushf
    pop ecx
    xor eax, ecx
    xor eax, ebx
    popf
    pop ebx
    pop ebp
    ret
cpuid_available endp

is_genuine_intel proc public
    push ebp
    mov ebp, esp
    push ebx
    mov eax, 0
    cpuid
    cmp ebx, 756e6547h
    jne vendor_different
    cmp edx, 49656e69h
    jne vendor_different
    cmp ecx, 6c65746eh
    jne vendor_different
    mov eax, 1h
    pop ebx
    pop ebp
    ret
is_genuine_intel endp

is_authentic_amd proc public
    push ebp
    mov ebp, esp
    push ebx
    mov eax, 0
    cpuid
    cmp ebx, 68747541h
    jne vendor_different
    cmp edx, 69746e65h
    jne vendor_different
    cmp ecx, 444d4163h
    jne vendor_different
    pop ebx
    pop ebp
    ret
is_authentic_amd endp

vendor_different:
    mov eax, 0
    pop ebx
    ret

highest_eax_basic_info proc public
    push ebp
    mov ebp, esp
    push ebx
    mov eax, 0
    cpuid
    pop ebx
    pop ebp
    ret
highest_eax_basic_info endp

highest_eax_extended_info proc public
    push ebp
    mov ebp, esp
    push ebx
    mov eax, 80000000h
    cpuid
    pop ebx
    pop ebp
    ret
highest_eax_extended_info endp

end

