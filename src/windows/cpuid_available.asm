.code

cpuid_available proc public
    push rbx
    mov ebx, 200000h
    pushf
    pop rax
    push rax
    xor eax, ebx
    push rax
    popfq
    pushfq
    pop rcx
    xor eax, ecx
    xor eax, ebx
    popf
    pop rbx
    ret
cpuid_available endp

is_genuine_intel proc public
    push rbx
    mov eax, 0
    cpuid
    cmp ebx, 756e6547h
    jne vendor_different
    cmp edx, 49656e69h
    jne vendor_different
    cmp ecx, 6c65746eh
    jne vendor_different
    mov eax, 1h
    pop rbx
    ret
is_genuine_intel endp

is_authentic_amd proc public
    push rbx
    mov eax, 0
    cpuid
    cmp ebx, 68747541h
    jne vendor_different
    cmp edx, 69746e65h
    jne vendor_different
    cmp ecx, 444d4163h
    jne vendor_different
    pop rbx
    ret
is_authentic_amd endp

vendor_different:
    mov eax, 0
    pop rbx
    ret

highest_eax_basic_info proc public
    push rbx
    mov eax, 0
    cpuid
    pop rbx
    ret
highest_eax_basic_info endp

highest_eax_extended_info proc public
    push rbx
    mov eax, 80000000h
    cpuid
    pop rbx
    ret
highest_eax_extended_info endp

end
