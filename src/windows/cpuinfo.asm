.data
.code

e_cpuid proc public
    push rbp
    mov rbp, rsp
    sub rbp, 32
    push rbx
    push r8
    mov eax, ecx
    mov ecx, edx
    cpuid
    pop r9
    mov dword ptr [r9], eax
    mov dword ptr [r9+4], ebx
    mov dword ptr [r9+8], ecx
    mov dword ptr [r9+12], edx
    pop rbx
    pop rbp
    ret
e_cpuid endp

get_cpu_name proc public
    push rbp
    mov rbp, rsp
;   Save destination pointer because cpuid will overwrite it.
    push rcx
;   Save original values of rbx and rdi, then allocate space for local char array.
    push rbx
    push rdi
    sub rsp, 80h
;   Save current stack offset to reference the char array, and initialize the index to 0.
    mov r8, rsp
    mov r9, 0h
;   Call cpuid for EAX=8000 0002-0004 and save the data from the registers to the
;   local array.
    mov eax, 80000002h
    cpuid
    call copy_brand_string
;   We're copying 16 chars at a time (4 registers, 4 chars/register), so increase local
;   array index by 16.
    add r9, 10h
    mov eax, 80000003h
    cpuid
    call copy_brand_string
    add r9, 10h
    mov eax, 80000004h
    cpuid
    call copy_brand_string
;   Reset local array index (r9) and destiation index (rcx)
    mov rcx, 0h
    mov r9, 0h
;   Get the destination pointer and put it in rdi
    mov rdi, [rbp-8]
start_skip_leading_spaces:
;   Increase source index until first non-space character found
    mov al, byte ptr [r8+r9]
    cmp al, 20h
    jne copy_cpu_name_to_dest
    inc r9
;   Check source index stays under 48 bytes since it will never get bigger than that.
    cmp r9, 30h
    jl start_skip_leading_spaces
start_copy_cpu_name_to_dest:
;   Copy byte found in r8[r9] into temporary register al
    mov al, byte ptr [r8+r9]
copy_cpu_name_to_dest:
;   Copy data from temporary register to dest[rcx]
    mov byte ptr [rdi+rcx], al
;   Increase both indexes, and check that source index stays under 48 bytes, otherwise
;   we're done.
    inc r9
    inc rcx
    cmp r9, 30h
    jl start_copy_cpu_name_to_dest
;   Dealloc stack
    add rsp, 80h
;   Get original values and return
    pop rdi
    pop rbx
    pop rcx
    pop rbp
    ret
get_cpu_name endp

copy_brand_string proc
    mov dword ptr [r8+r9], eax
    mov dword ptr [r8+r9+4], ebx
    mov dword ptr [r8+r9+8], ecx
    mov dword ptr [r8+r9+12], edx
    ret
copy_brand_string endp

end
